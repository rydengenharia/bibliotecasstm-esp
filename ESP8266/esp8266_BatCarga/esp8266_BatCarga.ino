#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <WiFiClient.h>
#include <SPI.h>
#include<SD.h>
#include <WiFiManager.h>


// Site remoto - Coloque aqui os dados do site que vai receber a requisição GET
//const char http_site[] = "http://localhost/Web_Server_NodeMCU/";
const int http_port = 80;
byte mac[6];
String macstr = "";
byte img = 0;
byte tamanho = 0;
const char* host = "192.168.21.151";

char last = 'a';
boolean flagSD = false;
boolean flagConexao = false;
boolean pular = false;

byte bufferImg[2000];
String bufferTxt = "";
String Lista = "";
int BufferImgLength;

int ledAlerta = 13;
File root;

uint32_t sleep_time_s = 60 * 1000000;

File myFile;

//ESP8266WebServer server(80);
WiFiServer server(80);

WiFiClient client;
WiFiManager wifiManager;

void setup() {
  pinMode(D1, OUTPUT);  //FLAG DE COMUNICAÇÃO ESP-STM
  digitalWrite(D1, LOW);
  pinMode(D2, OUTPUT);
  digitalWrite(D2, HIGH);

  Serial.begin(115200);
  bufferTxt.reserve(3000);
  Lista.reserve(3000);

  //----------------------------CONFIGURAÇÃO CONEXÃO---------------------------------//

  //  Promovendo configuração de acesso à rede local
  wifiManager.autoConnect("BatCargaConnect");
  flagConexao = !digitalRead(D1);
  Serial.println("Conectado");


  // Aguardando conectar na rede
  Serial.println("");
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print('.');
  }
  
  Serial.println(' ');
  //Mostrnado MAC
  WiFi.macAddress(mac);

  macstr += String(mac[0], DEC);
  //macstr += ":";
  macstr += String(mac[1], DEC);
  //macstr += ":";
  macstr += String(mac[2], DEC);
  //macstr += ":";
  macstr += String(mac[3], DEC);
  //macstr += ":";
  macstr += String(mac[4], DEC);
  //macstr += ":";
  macstr += String(mac[5], DEC);


  Serial.println("MAC Address: " + macstr);

  // Mostrando IP
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.print("Host: ");
  Serial.println(host);

  // Apenas informando que servidor iniciou
  Serial.println("Web Server Iniciado");


  server.begin();

  //----------------------------FIM CONFIGURAÇÃO CONEXÃO---------------------------------//


  //----------------------------CONFIGURAÇÃO SD CARD-------------------------------------//
  if (!SD.begin(4)) {
    Serial.println("Inicialização Falhou!");
    pinMode(D5, INPUT);
    pinMode(D6, INPUT);
    pinMode(D7, INPUT);
    pinMode(D8, INPUT);

    digitalWrite(D1, HIGH);
    digitalWrite(D2, LOW);

    ESP.deepSleep(0);
    return;
  }
  Serial.println("Inicialização Pronta.");


  //----------------------------FIM CONFIGURAÇÃO SD CARD---------------------------------//

}

void loop() {
  digitalWrite(ledAlerta, LOW);
  if (getLista(macstr) && flagConexao) {

    int a = 5;
    int b = Lista.indexOf(",", a);

    int count = Lista.substring(a, b).toInt();
    a = b;

    for (int i = 0; i < count; i++) {
      a = Lista.indexOf("(", a);
      b = Lista.indexOf(",", a);

      int id = Lista.substring(a + 1, b).toInt();

      a = b;
      b = Lista.indexOf(",", b + 1);
      String nome = Lista.substring(a + 1, b);

      a = b;
      b = Lista.indexOf(")", b + 1);
      int quantidade = Lista.substring(a + 1, b).toInt();

      Serial.print(id);
      Serial.print("-");
      Serial.print(nome);
      Serial.print("-");
      Serial.println(quantidade);

      if (!verificaArquivos(nome)) {
        downloadImagem(id, quantidade, nome);
      }

    }
    Serial.println("Apagando desatualizados");
    apagarAnuncioDesatualizado();
    Serial.println("Agora terminou-------------------------");

  }

  pinMode(D5, INPUT);
  pinMode(D6, INPUT);
  pinMode(D7, INPUT);
  pinMode(D8, INPUT);

  digitalWrite(D1, HIGH);
  digitalWrite(D2, LOW);

  ESP.deepSleep(0);

  while (true) {
    digitalWrite(D2, LOW);
    delay(500);
    digitalWrite(D2, HIGH);
    delay(500);
  }
}

boolean getLista(String ID) {
  if (!client.connect(host, http_port)) {
    Serial.println("Conexão Falhou!");
    return false;
  }

  String requisicao = "/Web_Server_NodeMCU/solicitaLista.php";
  requisicao += "?ID=" + ID;


  //Enviando requisição ao servidor
  client.print(String("GET ") + requisicao + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" +
               "Connection: close\r\n\r\n ");


  unsigned long timeout = millis();
  while (client.available() == 0) {
    if (millis() - timeout > 5000) {
      Serial.println("Client TimeOUT!!");
      BufferImgLength = 0;
      return false;
    }
  }

  //Leitura da resposta do servidor
  while (client.available()) {
    Lista += (char)client.read();
  }

  int inicio = Lista.indexOf("(QTD:");
  int fim = Lista.indexOf("FINAL)", inicio) + 6;
  if (inicio < 0 || fim < 0) {
    return false;
  }
  Lista = Lista.substring(inicio, fim);
  Lista.toUpperCase();
  return true;
}


boolean getPKG(int ID, int PKG) {
  if (!client.connect(host, http_port)) {
    Serial.println("Conexão Falhou!");
    return false;
  }
  String requisicao = "/Web_Server_NodeMCU/solicitaDownload.php";
  requisicao += "?ID=" + String(ID, DEC) + "&PKG=" + String(PKG, DEC);


  //Enviando requisição ao servidor
  client.print(String("GET ") + requisicao + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" +
               "Connection: close\r\n\r\n ");


  unsigned long timeout = millis();
  while (client.available() == 0) {
    if (millis() - timeout > 5000) {
      Serial.println("Client TimeOUT!!");
      return false;
    }
  }

  bufferTxt = "";
  //Leitura da resposta do servidor
  while (client.available()) {
    bufferTxt += (char)client.read();
  }
  BufferImgLength = 0;
  int inicio = bufferTxt.indexOf("(INICIO,") + 8;
  int fim = bufferTxt.indexOf(")", inicio);
  BufferImgLength = bufferTxt.substring(inicio, fim).toInt();

  fim++;
  for (int i = 0; i < BufferImgLength; i++) {
    bufferImg[i] = (byte)bufferTxt.charAt(fim + i);
  }
  return true;
}

void downloadImagem(int ID, int qtdPKG, String nome) {
  myFile = SD.open(nome, FILE_WRITE);

  if (!myFile) {
    Serial.println("Erro ao criar arquivo");
    return;
  }
  for (int i = 0; i <= qtdPKG; i++) {
    while (!getPKG(ID, i)) {
      Serial.println("Tentando de novo");
      delay(10000);
    }

    //    for (int j = 0; j < BufferImgLength; j++) {
    //      Serial.print(bufferImg[j], HEX);
    //      Serial.print(" ");
    //    }

    for (int j = 0; j < BufferImgLength; j++) {
      myFile.write(bufferImg[j]);
    }

    Serial.print(i * 100.0 / (double)qtdPKG);
    Serial.print("\t");
    Serial.println(BufferImgLength);

  }
  myFile.close();
  Serial.println("Terminou");

}

boolean verificaArquivos(String nomeArquivo) {
  File anuncio = SD.open(nomeArquivo);
  if (!anuncio) {
    Serial.println("Anuncio nao encontrado no SD Card");
    return false;
  } else {
    Serial.println("Anuncio ja disponivel no SD Card");
    return true;
  }
}

void apagarAnuncioDesatualizado() {
  root = SD.open("/");
  root.rewindDirectory();
  while (true) {
    File arquivo = root.openNextFile();

    if (!arquivo) {
      break;
    }
    if (!arquivo.isDirectory()) {
      if (Lista.indexOf(arquivo.name()) < 0) {
        Serial.println(arquivo.name());
        arquivo.close();
        SD.remove(arquivo.name());
      } else {
        arquivo.close();
      }
    } else {
      arquivo.close();
    }
  }
  root.close();
}



