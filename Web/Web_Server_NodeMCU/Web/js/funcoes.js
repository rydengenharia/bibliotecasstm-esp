//Devolve data do tipo date completo quebrando strings do tipo (YYYY-MM-DD HH:MM:SS)
function devolveData_obsoleta(string) {
    var ano, mes, dia, hora, minuto, segundo, data;
    ano = parseInt(string.substring(0, 4));

    //removendo zero a esquerda
    if (string.substring(5, 6) == '0') {
        mes = parseInt(string.substring(6, 7));
    } else {
        mes = parseInt(string.substring(5, 7));
    }

    if (string.substring(8, 9) == '0') {
        dia = parseInt(string.substring(9, 10));
    } else {
        dia = parseInt(string.substring(8, 10));
    }

    if (string.substring(11, 12) == '0') {
        hora = parseInt(string.substring(12, 13));
    } else {
        hora = parseInt(string.substring(11, 13));
    }

    if (string.substring(14, 15) == '0') {
        minuto = parseInt(string.substring(15, 16));
    } else {
        minuto = parseInt(string.substring(14, 16));
    }

    if (string.substring(17, 18) == '0') {
        segundo = parseInt(string.substring(18, 19));
    } else {
        segundo = parseInt(string.substring(17, 19));
    }

    data = new Date();
    data.setHours(hora);
    data.setMinutes(minuto);
    data.setSeconds(segundo);
    return data;
}


//Definindo variaveis constante para todos
var sensorAtual, usuarioAtual, diaInicioAtual, diaFinalAtual, XMLdoc;
var mapa;
var propriedadesMapa = {
    center: new google.maps.LatLng(-16.680695, -49.264487),
    zoom: 5,
    mapTypeId: google.maps.MapTypeId.ROADMAP
};
var grafico;

document.getElementById("botaoatualiza").disabled = "disabled";

//Atualiza o diainicio e o dia fim
function atualizaDatas(){
    var inicioDefinido = false;
    var fimDefinido = false;

    diaInicioAtual = document.getElementById("diainicio").value;
    diaFinalAtual = document.getElementById("diafim").value;
    document.getElementById("botaoatualiza").disabled = "disabled";

    //verificando se as duas datas estao definidas
    if(diaInicioAtual != ""){
        inicioDefinido = true;
    }
    if(diaFinalAtual != ""){
        fimDefinido = true;
    }

    //ativando botao
    if(diaInicioAtual && diaFinalAtual){
        document.getElementById("botaoatualiza").disabled = "";
    }
}

//Define o texto de ajuda
function textoAjuda(nome){
    var ajuda = document.getElementById("ajuda");
    switch (nome){
        case "barra_deslizante":
                ajuda.innerText = "Deslize a barra para aumentar ou diminuir a quantidade de pontos no gráfico!";
    }

    if(typeof(nome) == 'undefined'){
        ajuda.innerText = "";
    }
}

//Define limites do elemento range
function defineMaxRangeSlidebar(max){
    document.getElementById("slidebar").max = max;
    document.getElementById("rangemax").innerText = document.getElementById("slidebar").max;
}

//Define a temperatura para o usuario atual | Ultima medição feita
function defineTemperatura(){
    var XML = retornaXMLSensorEspecifico("temperatura");
    var temperaturas_selecionadas = XML.getElementsByTagName("TEMPERATURA");
    if(temperaturas_selecionadas.length > 0){
        document.getElementById("temperatura_atual").innerText = temperaturas_selecionadas[temperaturas_selecionadas.length-1].childNodes[0].nodeValue + " ºC";
    }else{
        document.getElementById("temperatura_atual").innerText = "-- ºC";
    }
}

//Define o peso e a altura para o usuario atual | Ultima medição feita
function definePesoAltura(){
    var XML = retornaXMLSensorEspecifico("pesoaltura");
    var pesos_selecionados = XML.getElementsByTagName("PESO");
    var alturas_selecionadas = XML.getElementsByTagName("ALTURA");

    if(pesos_selecionados.length > 0){
        document.getElementById("peso_atual").innerText = pesos_selecionados[pesos_selecionados.length-1].childNodes[0].nodeValue + " kg";
    }else{
        document.getElementById("peso_atual").innerText = "-- Kg";
    }

    if(alturas_selecionadas.length > 0){
        document.getElementById("altura_atual").innerText = alturas_selecionadas[alturas_selecionadas.length-1].childNodes[0].nodeValue + " m";
    }else{
        document.getElementById("altura_atual").innerText = "-- m";
    }
}

//Define a pressao para o usuario atual | Ultima medição
function definePressao(){
    var XML = retornaXMLSensorEspecifico("pressao");
    var sistoles_selecionadas = XML.getElementsByName("SISTOLE");
    var diastoles_selecionadas = XML.getElementsByTagName("DIASTOLE");

    if(sistoles_selecionadas.length > 0){
        document.getElementById("sistole_atual").innerText = sistoles_selecionadas[sistoles_selecionadas.length-1].childNodes[0].nodeValue + " mmHg";
    }else{
        document.getElementById("sistole_atual").innerText = "-- mmHg";
    }
    if(diastoles_selecionadas.length > 0){
        document.getElementById("diastole_atual").innerText = diastoles_selecionadas[diastoles_selecionadas.length-1].childNodes[0].nodeValue + " mmHg";
    }else{
        document.getElementById("diastole_atual").innerText = "-- mmHg";
    }
}

//Define a glicemia para o usuario atual | Ultima medição
function defineGlicemia(){
    var XML = retornaXMLSensorEspecifico("glicose");
    var glicemias_selecionadas = XML.getElementsByTagName("GLICEMIA");

    if(glicemias_selecionadas.length > 0){
        document.getElementById("glicemia_atual").innerText = glicemias_selecionadas[glicemias_selecionadas.length-1].childNodes[0].nodeValue + " mg/dL";
    }else{
        document.getElementById("glicemia_atual").innerText = "-- mg/dL";
    }
}

//Define qual é o sensor clicado nas abas
function defineSensorAtual(sensor){
    sensorAtual = sensor.toLowerCase();
}

//Retorna qual é o ultimo sensor clicado.
function retornaSensorAtual(){
    return sensorAtual;
}

//Retorna um array com os tipos de dado que o sensor possui
function retornaTipoDeDadoDoSensor(){
    var dado = [];
    switch (retornaSensorAtual()){
        case "acelerometro":
            dado.push("Media");
            break;
        case "glicose":
            dado.push("Glicemia");
            break;
        case "temperatura":
            dado.push("Temperatura");
            break;
        case "pressao":
            dado.push("Sistole");
            dado.push("Diastole");
            break;
        default:
            alert("Este sensor não possui dados gráficos ainda.");
            dado.push("Nenhum dado para apresentar.");
    }
    return dado;
}

//Define usuário atual
function defineUsuarioAtual(usuario){
    usuarioAtual = usuario.toLowerCase();
}

//Retorna o ultimo usuario
function retornaUsuarioAtual(){
    return usuarioAtual;
}

//Carrega arquivo XML na variavel XMLdoc
function carregaXMLdoc() {
    var xhttp;
    if (window.XMLHttpRequest) {
        xhttp = new XMLHttpRequest();
    }
    else {
        //Codigos para browsers antigos: IE5 and IE6
        xhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xhttp.open("GET", "views/geraXML.php?usuario=" + retornaUsuarioAtual().toLowerCase() + "&sensor=" + retornaSensorAtual().toLowerCase(), false);
    xhttp.send();
    XMLdoc = xhttp.responseXML;
}

//Carrega arquivo XML na variavel XMLdoc
function retornaXMLSensorEspecifico(sensor) {
    var xhttp;
    if (window.XMLHttpRequest) {
        xhttp = new XMLHttpRequest();
    }
    else {
        //Codigos para browsers antigos: IE5 and IE6
        xhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xhttp.open("GET", "views/geraXML.php?usuario=" + retornaUsuarioAtual() + "&sensor=" + sensor.toLowerCase(), false);
    xhttp.send();
    return xhttp.responseXML;
}

//Formata data para DD-MM-AAAA e retorna a string
function formataDataString(data){
    if(data.substring(4, 5) == " "){
        return data.split(" ").join("-");
    }else{
        return data.substring(0, 10);
    }
}

//Formata para o tipo de variavel Date() do JavaScript
function formataParaTipoDate(string) {
    var ano, mes, dia, hora, minuto, segundo, data;
    ano = parseInt(string.substring(0, 4));

    //removendo zero a esquerda
    if (string.substring(5, 6) == '0') {
        mes = parseInt(string.substring(6, 7));
    } else {
        mes = parseInt(string.substring(5, 7));
    }

    if (string.substring(8, 9) == '0') {
        dia = parseInt(string.substring(9, 10));
    } else {
        dia = parseInt(string.substring(8, 10));
    }

    if (string.substring(11, 12) == '0') {
        hora = parseInt(string.substring(12, 13));
    } else {
        hora = parseInt(string.substring(11, 13));
    }

    if (string.substring(14, 15) == '0') {
        minuto = parseInt(string.substring(15, 16));
    } else {
        minuto = parseInt(string.substring(14, 16));
    }

    if (string.substring(17, 18) == '0') {
        segundo = parseInt(string.substring(18, 19));
    } else {
        segundo = parseInt(string.substring(17, 19));
    }
    data = new Date();
    data.setYear(ano);
    data.setMonth(mes);
    data.setDate(dia);
    data.setHours(hora);
    data.setMinutes(minuto);
    data.setSeconds(segundo);

    return data;
}

//Inicializa configurações do mapa
function inicializaMapa(){
    mapa = new google.maps.Map(document.getElementById("conteudo"), propriedadesMapa);
}

//Inicializa configuraçoes do grafico
function inicializaGrafico(){
    grafico = new CanvasJS.Chart("conteudo", {
        zoomEnabled: true,
        panEnabled: true
    });

    carregaXMLdoc();

    //Definindo nome das retas
    var axisX = "Tempo";
    var elemento_axisY = XMLdoc.getElementsByTagName("AXISY");
    var axisY = elemento_axisY[0].childNodes[0].nodeValue;
    var elemento_suffixY = XMLdoc.getElementsByTagName("SUFFIXY");
    var suffixY = elemento_suffixY[0].childNodes[0].nodeValue;
    grafico.options.axisX = {
        title: axisX,
        prefix: "",
        suffix: ""
    };
    grafico.options.axisY = {
        title: axisY,
        prefix: "",
        suffix: suffixY
    };
    grafico.options.zoomEnabled = true;
    grafico.options.title = { text: retornaSensorAtual().toUpperCase() };
    grafico.options.data = [];

}

//Adiciona os pontos no gráfico
function adicionaDadosGrafico(quantidade){
    var dado, dados_selecionados, valor, rangeMax = 0, datas_selecionadas;

    dado = retornaTipoDeDadoDoSensor();
    diaInicioAtual = formataDataString(diaInicioAtual);
    diaFinalAtual = formataDataString(diaFinalAtual);

    inicializaGrafico();

    //Para cada tipo de dados retornado
    for(var a = 0; a < dado.length; a++){

        var serie = {
            type: "line",
            name: dado[a],
            showInLegend: true,
            toolTipContent: "{name}: {y} <br> {x}"
        };

        serie.dataPoints = [];

        dados_selecionados = XMLdoc.getElementsByTagName(dado[a].toUpperCase());
        datas_selecionadas = XMLdoc.getElementsByTagName("TEMPO");

        //Procurando data inicial
        for(var d = 0; d < datas_selecionadas.length; d++){
            if(diaInicioAtual <= formataDataString(datas_selecionadas[d].childNodes[0].nodeValue)){
                diaInicioAtual = formataDataString(datas_selecionadas[d].childNodes[0].nodeValue);
                break;
            }
        }

        if(typeof(quantidade) == 'undefined'){

            for(var b = d; b < datas_selecionadas.length; b++){

                if(formataDataString(datas_selecionadas[b].childNodes[0].nodeValue) <= diaFinalAtual){

                    valor = parseFloat(dados_selecionados[b].childNodes[0].nodeValue);
                    serie.dataPoints.push({y: valor, name: dado[a], x: formataParaTipoDate(datas_selecionadas[b].childNodes[0].nodeValue)});
                    rangeMax++;
                }
            }
            defineMaxRangeSlidebar(rangeMax);
        }else{

            var j = 0;

            for(var c = d; c < datas_selecionadas.length; c++){

                if(formataDataString(datas_selecionadas[c].childNodes[0].nodeValue) <= diaFinalAtual && j < quantidade){

                    valor = parseFloat(dados_selecionados[c].childNodes[0].nodeValue);
                    serie.dataPoints.push({y: valor, name: dado[a], x: formataParaTipoDate(datas_selecionadas[c].childNodes[0].nodeValue)});
                    j++;
                }
            }

            for(var e = d; e < datas_selecionadas.length; e++){
                if(formataDataString(datas_selecionadas[e].childNodes[0].nodeValue) <= diaFinalAtual){

                    rangeMax++;
                }
            }
            defineMaxRangeSlidebar(rangeMax);
        }
        grafico.options.data.push(serie);
        grafico.render();
    }
}

//Função que chama funções de acordo com o tipo do sensor
function chamarFuncoes(sensor){
    defineSensorAtual(sensor);
    if(sensorAtual == 'pressao' || sensorAtual == 'glicose' || sensorAtual == 'acelerometro' || sensorAtual == 'temperatura'){
        adicionaDadosGrafico(5);
    }
}

//Funções antigas
function adicionaMarcador(latitude, longitude) {
    var myCenter = new google.maps.LatLng(latitude, longitude);
    var marker = new google.maps.Marker({
        position: myCenter
    });
    marker.setMap(map);
}
function carregarGPS(usuario, quantidade) {
    var XMLdoc = loadXMLDoc("views/geraXML.php?usuario=" + usuario + "&sensor=gps");
    var selected_latitude = XMLdoc.getElementsByTagName("LATITUDE");
    var selected_longitude = XMLdoc.getElementsByTagName("LONGITUDE");
    if (quantidade <= selected_latitude.length && quantidade > 0) {
        for (var i = 0; i < quantidade; i++) {
            var lat = parseFloat(selected_latitude[i].childNodes[0].nodeValue);
            var lon = parseFloat(selected_longitude[i].childNodes[0].nodeValue);
            adicionaMarcador(lat, lon);
        }
    } else {
        alert("Quantidade de pontos no banco de GPS inválida, escolha uma quantidade positiva e menor ou igual ao número de dados adicionados ao banco.");
    }
    return selected_latitude.length;
}