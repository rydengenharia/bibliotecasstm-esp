//Adiciona uma notificação flutuante
var notificacao_vermelha = function(texto) {
    $.gritter.add({
        position: 'top-right',
        title: 'Notificação',
        text: texto,
        time: '',
        class_name: 'danger'
    });
};
var notificacao_verde = function(texto) {
    $.gritter.add({
        position: 'top-right',
        title: 'Notificação',
        text: texto,
        time: '',
        class_name: 'success'
    });
};

//Aplicativo do painel
var formApp = angular.module('formApp', []);

formApp.controller('loginController', ['$scope', '$http', function($scope, $http) {
        this.login = null;
        this.senha = null;

        this.submit = function() {
            $http
                    .get('controllers/login.controller.php?login=' + this.login + '&senha=' + this.senha)
                    .success(function(data) {
                        if (data === 'done') {
                            window.location.reload(true);
                        } else {
                            notificacao_vermelha(data);
                        }
                    });
        };
    }]);

formApp.controller('cadastroController', ['$scope', '$http', function($scope, $http) {
        this.nomeCompleto = null;
        this.email1 = null;
        this.email2 = null;
        this.cpf = null;
        this.usuario = null;
        this.senha = null;
        this.senhaConfirm = null;

        this.submit = function() {
            $http({
                method: 'post',
                url: 'controllers/cadastro.usuario.controller.php',
                data: {
                    cpf: this.cpf,
                    nome_completo: this.nomeCompleto,
                    login: this.usuario,
                    senha: this.senha,
                    senha_confirm: this.senhaConfirm,
                    email1: this.email1,
                    email2: this.email2
                }
            }).success(function(retorno) {
                if (retorno === 'done') {
                    notificacao_verde("Usuário cadastrado!");
                    window.location.href = "login.php";
                } else {
                    notificacao_vermelha(retorno);
                }
            });
        };
    }]);

formApp.controller('recuperaSenhaController', ['$scope', '$http', function($scope, $http) {
        this.email = null;
        $scope.status = '';

        this.enviaEmail = function() {
            $scope.status = 'Processando pedido...';
            $http({
                method: 'get',
                url: 'controllers/email.recuperacao.senha.controller.php?email=' + this.email,
            }).success(function(retorno) {
                $scope.status = '';
                if (retorno.length < 10) {
                    notificacao_verde("Email enviado!");
                } else {
                    notificacao_vermelha(retorno);
                }
            });
        };

        this.alteraSenha = function() {
            if ($scope.senha === $scope.senha2) {
                $http({
                    method: 'post',
                    url: 'controllers/altera.recuperacao.senha.controller.php',
                    data: $.param({senha: $scope.senha}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(retorno) {
                    if(retorno === 'done'){
                        notificacao_verde("Senha alterada!");
                    }else{
                        notificacao_vermelha(retorno);
                    }
                });
            } else {
                notificacao_vermelha("Os valores dos campos não coincidem!");
            }
        };
    }]);