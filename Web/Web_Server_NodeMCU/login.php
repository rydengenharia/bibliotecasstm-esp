<!DOCTYPE html>
<?php
session_start();
if(isset($_SESSION['USER_ID'])){
  //  header("Location: painel-principal.php");
}
?>

<html lang="pt">
<head>
    <title>BatCarga - Login</title>
    <link rel="shortcut icon" href="Web/img/batcarga.ico"/>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" href="Web/css/bootstrap.css">
    <link rel="stylesheet" href="Web/css/bootstrap-theme.css">
    <link rel="stylesheet" href="Web/js/jquery.gritter/css/jquery.gritter.css">
    <style rel="stylesheet" href="Web/css/style.css" type="text/css">
        body {
            margin-top: 200px;
            background-size: 300px;
            color: #000000;
            background: #FFFFFF url("Web/img/batcarga.png") no-repeat top;
        }
    </style>
</head>

<body>
<script type="text/javascript" src="Web/js/jquery.js"></script>
<script type="text/javascript" src="Web/js/jquery.form.js"></script>
<script type="text/javascript" src="Web/js/bootstrap.js"></script>
<script type="text/javascript" src="Web/js/jquery.gritter/js/jquery.gritter.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

        //Mostra a tooltip sobre o formato do CPF
        $("#cpf").tooltip();

        //Verifica se os campos estão vazios, caso esteja deixa vermelho
        function adicionaWarning(id) {
            $("#" + id).blur(function () {
                if ($(this).val().length == 0) {
                    $("#input-" + this.id).addClass("has-warning");
                    $("#icon-" + this.id).addClass("hidden");
                } else {
                    $("#input-" + this.id).removeClass("has-warning").addClass("has-success");
                    $("#icon-" + this.id).removeClass("hidden");
                }
            });
        }

        //Adicionando o aviso aos campos
        adicionaWarning("nomecompleto");
        adicionaWarning("email1");
        adicionaWarning("cpf");
        adicionaWarning("usuario-cadastro");
        adicionaWarning("senha-cadastro");
        adicionaWarning("usuario-login");
        adicionaWarning("senha-login");

        //Envia o cadastro com método POST por Ajax
        $("#botao-cadastro").click(function () {
            $("#form-cadastro").ajaxForm({
                url: "config-cadastro-action.php",
                type: "post",
                success: cadastroCallBack
            });
        });
        function cadastroCallBack(data) {
            if(data == "Sucesso!"){
                $.gritter.add({
                    position: 'top-right',
                    title: 'Notificação',
                    text: 'Usuário cadastrado com sucesso.',
                    time: ''
                });
                $("#form-cadastro").resetForm();
            }else{
                $.gritter.add({
                    position: 'top-right',
                    title: 'Notificação',
                    text: data,
                    time: ''
                });
            }
        }

        //Envia o login por POST
        $("#botao-login").click(function () {
            $("#form-login").ajaxForm({
                url: "config-login.php",
                type: "post",
                success: loginCallBack
            });
        });
        function loginCallBack(data) {
            if(data == "Sucesso!"){
                window.location.replace("painel-principal.php");
            }else{
                $.gritter.add({
                    position: 'top-right',
                    title: 'Notificação',
                    text: data,
                    time: ''
                });
            }
        }
    });
</script>
<div class="container" style="width: 500px;">
    <div class="row">
        <div class="col-md-12 center-block">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="nav nav-pills" role="tablist">
                                <li id="tab-botao-login" class="active"><a href="#tab-login" data-toggle="tab">Login</a></li>
                                <li><a href="#tab-cadastro" data-toggle="tab">Cadastro</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-content">
                        <div id="tab-login" class="tab-pane active">
                            <div id="panel" class="panel panel-default">
                                <div class="panel-body">
                                    <form id="form-login" class="form-horizontal" role="form">
                                        <div id="input-usuario-login" class="form-group has-feedback">
                                            <label for="usuario-login" class="col-sm-4 control-label">Usuário:</label>

                                            <div class="col-sm-8">
                                                <input id="usuario-login" type="text" name="usuario" class="form-control" placeholder="Seu usuário"/>
                                                <span id="icon-usuario" class="hidden glyphicon glyphicon-ok form-control-feedback"></span>
                                            </div>
                                        </div>
                                        <div id="input-senha-login" class="form-group has-feedback">
                                            <label for="senha-login" class="col-sm-4 control-label">Senha:</label>

                                            <div class="col-sm-8">
                                                <input id="senha-login" type="password" name="senha"
                                                       class="form-control"
                                                       placeholder="Sua senha"/>
                                                <span id="icon-senha"
                                                      class="hidden glyphicon glyphicon-ok form-control-feedback"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-4"></div>
                                            <div class="col-sm-8">
                                                <button id="botao-login" class="form-control btn btn-primary" type="submit">Entrar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div id="tab-cadastro" class="tab-pane">
                            <div class="panel panel-default">
                                <div class="panel-body" role="form">
                                    <div id="alerta" class="alert alert-danger hidden fade in">
                                        <button type="button" class="close" onclick="$('.alert').hide()">
                                            <span aria-hidden="true">&times;</span>
                                            <span class="sr-only">Fechar</span>
                                        </button>
                                        <div id="alerta-texto">
                                        </div>
                                    </div>
                                    <form id="form-cadastro" class="form-horizontal">
                                        <div id="input-nomecompleto" class="form-group has-feedback">
                                            <label for="nomecompleto" class="col-sm-4 control-label">Nome
                                                Completo:</label>
                                            <div class="col-sm-8">
                                                <input id="nomecompleto" type="text" name="nomecompleto" class="form-control" placeholder="Seu nome completo"/>
                                                <span id="icon-nomecompleto" class="hidden glyphicon glyphicon-ok form-control-feedback"></span>
                                            </div>
                                        </div>
                                        <div id="input-email1" class="form-group has-feedback">
                                            <label for="email1" class="col-sm-4 control-label">Email Principal:</label>

                                            <div class="col-sm-8">
                                                <input id="email1" type="text" name="email1" class="form-control"
                                                       placeholder="Seu E-mail"/>
                                                <span id="icon-email1" class="hidden glyphicon glyphicon-ok form-control-feedback"></span>
                                            </div>
                                        </div>
                                        <div id="input-email2" class="form-group has-feedback">
                                            <label for="email2" class="col-sm-4 control-label">Email Secundário:</label>

                                            <div class="col-sm-8">
                                                <input id="email2" type="text" name="email2" class="form-control"
                                                       placeholder="E-mail secundário"/>
                                                <span id="icon-email2" class="hidden glyphicon glyphicon-ok form-control-feedback"></span>
                                            </div>
                                        </div>
                                        <div id="input-cpf" class="form-group has-feedback">
                                            <label for="cpf" class="col-sm-4 control-label">CPF:</label>

                                            <div class="col-sm-8">
                                                <input id="cpf" type="text" name="cpf" class="form-control" placeholder="Seu CPF" data-toggle="tooltip" data-placement="top" title="Digite somente os números!"/>
                                                <span id="icon-cpf"
                                                      class="hidden glyphicon glyphicon-ok form-control-feedback"></span>
                                            </div>
                                        </div>
                                        <div id="input-usuario-cadastro" class="form-group has-feedback">
                                            <label for="usuario-cadastro"
                                                   class="col-sm-4 control-label">Usuário:</label>

                                            <div class="col-sm-8">
                                                <input id="usuario-cadastro" type="text" name="usuario" class="form-control" placeholder="Seu usuário"/>
                                                <span id="icon-usuario" class="hidden glyphicon glyphicon-ok form-control-feedback"></span>
                                            </div>
                                        </div>
                                        <div id="input-senha-cadastro" class="form-group has-feedback">
                                            <label for="senha-cadastro" class="col-sm-4 control-label">Senha:</label>

                                            <div class="col-sm-8">
                                                <input id="senha-cadastro" type="password" name="senha" class="form-control" placeholder="Sua senha"/>
                                                <span id="icon-senha" class="hidden glyphicon glyphicon-ok form-control-feedback"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-4"></div>
                                            <div class="col-sm-8">
                                                <input id="botao-cadastro" class="form-control btn btn-primary" type="submit" value="Cadastrar"/>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
