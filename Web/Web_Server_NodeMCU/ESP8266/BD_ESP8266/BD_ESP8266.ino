#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <WiFiClient.h>
#include<SD.h>
File myFile;

// Site remoto - Coloque aqui os dados do site que vai receber a requisição GET
//const char http_site[] = "http://localhost/Web_Server_NodeMCU/";
const int http_port = 80;
byte mac[6];
String macstr = "";
byte img = 0;
byte tamanho = 0;
const char* host = "192.168.21.151";

//ESP8266WebServer server(80);
WiFiServer server(80);

const int chipSelect = 4;

void setup() {
  Serial.begin(9600);
  SD.begin(chipSelect);


  //----------------------------CONFIGURAÇÃO CONEXÃO---------------------------------//
  //Tratamento de Conexão
  // Iniciando WiFi
  WiFi.begin("CEI", "#CEI-ufg16%");
  IPAddress subnet(255, 255, 255, 0);
  WiFi.config(IPAddress(192, 168, 21, 125),
              IPAddress(192, 168, 21, 254), subnet);

  // Aguardando conectar na rede
  Serial.println("");
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print('.');
  }
  Serial.println(' ');
  //Mostrnado MAC
  WiFi.macAddress(mac);

  macstr += String(mac[0],DEC);
  //macstr += ":";
  macstr += String(mac[1],DEC);
  //macstr += ":";
  macstr += String(mac[2],DEC);
  //macstr += ":";
  macstr += String(mac[3],DEC);
  //macstr += ":";
  macstr += String(mac[4],DEC);
  //macstr += ":";
  macstr += String(mac[5],DEC);


  Serial.println("MAC Address: " + macstr);
  //  Serial.print(mac[0],HEX);
  //  Serial.print(":");
  //  Serial.print(mac[1],HEX);
  //  Serial.print(":");
  //  Serial.print(mac[2],HEX);
  //  Serial.print(":");
  //  Serial.print(mac[3],HEX);
  //  Serial.print(":");
  //  Serial.print(mac[4],HEX);
  //  Serial.print(":");
  //  Serial.println(mac[5],HEX);

  // Mostrando IP
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.print("Host: ");
  Serial.println(host);
  // Apenas informando que servidor iniciou
  Serial.println("Web Server Iniciado");


  server.begin();

  //----------------------------FIM CONFIGURAÇÃO CONEXÃO---------------------------------//


  //----------------------------CONFIGURAÇÃO SD CARD-------------------------------------//
  /*  pinMode(4, OUTPUT);
    Serial.println("\nInicializando SD Card...");

    if (!card.init(SPI_HALF_SPEED, chipSelect)) {
       Serial.println("Inicialização falhou");
        return;
    } else {
    Serial.println("Conexões corretas: SD Card conectado.");
    }
  */
  //----------------------------FIM CONFIGURAÇÃO SD CARD---------------------------------//

  // Start no servidor

}

void loop() {
  delay(1000);
  WiFiClient client;

  if (!client.connect(host, http_port)) {
    Serial.println("Conexão Falhou!");
    return;
  }

  String url = "/Web_Server_NodeMCU/BD/read.php";
  url += "?mac=";
  url += macstr;

  Serial.print("Requisitando URL: ");
  Serial.println(url);

  //Enviando requisição ao servidor
  client.print(String("GET ") + url + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" +
               "Connection: close\r\n\r\n ");


  unsigned long timeout = millis();
  while (client.available() == 0) {
    if (millis() - timeout > 5000) {
      Serial.println("Client TimeOUT!!");
      return;
    }
  }

  //Leitura da resposta do servidor
  while (client.available()) {
    String resp = client.readStringUntil('\r');
    Serial.print(resp);
  }

  Serial.println();
  Serial.println("Fechando Conexão!!");
  //------------------------------------- Leitura dos dados do SD Card--------------------//
  //TODO: ALTERAR

  //------------------------------------- Leitura dos dados do SD Card--------------------//

}


//// Executa o HTTP GET request no site remoto
//  bool getPage(int img, int tamanho) {
//    if ( !client.connect(server, http_port) ) {
//      Serial.println("Falha na conexao com o site ");
//      return false;
//    }
//    String param = "/?img=" + String(img) + "&tamanho=" + String(tamanho); //Parâmetros com as leituras
//    Serial.println(param);
//    client.println("GET /Web_Server_NodeMCU/BD/read.php" + param + " HTTP/1.1");
//    client.println("Host: ");
//    client.println(http_site);
//    client.println("Connection: close");
//    client.println();
//    client.println();
//
//      // Informações de retorno do servidor para debug
//    while(client.available()){
//      String line = client.readStringUntil('\r');
//      Serial.print(line);
//    }
//    return true;
//  }




