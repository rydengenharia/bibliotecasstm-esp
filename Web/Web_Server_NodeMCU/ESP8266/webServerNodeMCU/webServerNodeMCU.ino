#include <ESP8266WiFi.h>
#include <SPI.h>
#include <SD.h>

// Dados de autenticação do WiFi 
const char* ssid = "CEI"; 
const char* password = "#CEI-ufg16%";
WiFiServer server(80);

// Config. SD Card
Sd2Card card;
SdVolume volume;
SdFile root;
const int chipSelect = 4;
 
void setup() {
  delay(1000);
  pinMode(LED_BUILTIN, OUTPUT); //Led interno da placa. 
  
  Serial.begin(9600); // Vamos usar para debug
  SD.begin(chipSelect);  
 
  // Conectando à rede WiFi
  Serial.println();
  Serial.println();
  Serial.print("Conectando com: ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi conectado com sucesso");
 
  // Startando o servidor
  server.begin();
  Serial.print("Servidor startado em ");
  Serial.print("http://");
  Serial.print(WiFi.localIP());
  Serial.println("/");

  //----------------------------CONFIGURAÇÃO SD CARD-------------------------------------//
    pinMode(4, OUTPUT);
    Serial.println("\nInicializando SD Card...");
    
    if (!card.init(SPI_HALF_SPEED, chipSelect)) {
       Serial.println("Inicialização falhou");  
        return;
    } else {
   Serial.println("Conexões corretas: SD Card conectado."); 
  }
  
//----------------------------FIM CONFIGURAÇÃO SD CARD---------------------------------//

}
 
void loop() {
  // Aguardando requisições do cliente
  WiFiClient client = server.available();
  if (!client) {
    return;
  }
  // Leitura da requisição
  Serial.print("Mensagem do cliente: ");
  while(!client.available()) {
    delay(1);
  }
  String request = client.readStringUntil('\r');
  Serial.println(request);
  client.flush();

 //Respostas de acordo com requisição
  // Muda situação do LED, caso o parâmetro seja detectado 
  if (request.indexOf("ONOFF") != -1) {
    digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
  }

  if (request.indexOf("LER") != -1) {
    leituraSDCard();
    return;
  }
 
  //Resposta padrão
  client.println("HTTP/1.1 200 OK");
  client.println("Content-Type: text/html");
  client.println(""); 
  client.println("<!DOCTYPE HTML>");
  client.println("<html>");
  client.println("<head><title>ESP8266 Web Server Exemplo</title></head>");  
  client.println("<body>ESP8266 Web Server Exemplo</body>");
  client.println("</html>");
    
  delay(1);
}

void leituraSDCard(){

// print the type of card
  Serial.print("\nCard type: ");
  switch(card.type()) {
    case SD_CARD_TYPE_SD1:
      Serial.println("SD1");
      break;
    case SD_CARD_TYPE_SD2:
      Serial.println("SD2");
      break;
    case SD_CARD_TYPE_SDHC:
      Serial.println("SDHC");
      break;
    default:
      Serial.println("Unknown");
  }

  // Now we will try to open the 'volume'/'partition' - it should be FAT16 or FAT32
  if (!volume.init(card)) {
    Serial.println("Could not find FAT16/FAT32 partition.\nMake sure you've formatted the card");
    return;
  }


  // print the type and size of the first FAT-type volume
  uint32_t volumesize;
  Serial.print("\nVolume type is FAT");
  Serial.println(volume.fatType(), DEC);
  Serial.println();
  
  volumesize = volume.blocksPerCluster();    // clusters are collections of blocks
  volumesize *= volume.clusterCount();       // we'll have a lot of clusters
  volumesize *= 512;                            // SD card blocks are always 512 bytes
  Serial.print("Volume size (bytes): ");
  Serial.println(volumesize);
  Serial.print("Volume size (Kbytes): ");
  volumesize /= 1024;
  Serial.println(volumesize);
  Serial.print("Volume size (Mbytes): ");
  volumesize /= 1024;
  Serial.println(volumesize);

  
  Serial.println("\nArquivos Encontrados (Nome, Data e Tamanho em bytes): ");
  root.openRoot(volume);
  
  // list all files in the card with date and size
  root.ls(LS_R | LS_DATE | LS_SIZE);
  
  }
